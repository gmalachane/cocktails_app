import { StatusBar } from 'expo-status-bar';
import { FlatList, StyleSheet, Text, View, Button, Image, ScrollView, TouchableOpacity } from 'react-native';
import React, {useEffect, Component, useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useNavigation } from '@react-navigation/native';
import { FullWindowOverlay } from 'react-native-screens';



const NavbarView = ({}) => {
    const navigation = useNavigation();

    return (
    <View style={styles.navbar}><TouchableOpacity onPress={() => {navigation.navigate('Home')}}><Image source={require('../assets/accueil.png')} style={styles.navbar_item}/></TouchableOpacity><TouchableOpacity onPress={() => {navigation.navigate('Favoris', { id : '' })}}><Image source={require('../assets/favoris.png')} style={styles.navbar_item}/></TouchableOpacity></View>
    )
}

const styles = StyleSheet.create({

    navbar: {
        alignItems: 'center', 
        justifyContent: 'space-evenly',
        height: 50,
        flexDirection: 'row',
    },
    navbar_item: {
        height: 30,
        width: 30,
        shadowColor: 'grey',
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius:1,

    }
    });

export default NavbarView