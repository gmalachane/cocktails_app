import { StatusBar } from 'expo-status-bar';
import { ImageBackground, FlatList, StyleSheet, Text, View, Button, Image, ScrollView } from 'react-native';
import React, {useEffect, Component, useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';



const DetailsView = ({route}) => {

    const [data, setData] = useState({});
    console.log(route.params.id)
    useEffect(() => {
        (async () => {
          fetchDataDetails();
        })();
      }, [])
      
    const fetchDataDetails = () => {
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${route.params.id}`).then(res => res.json()).then(data => {
        setData(data.drinks[0]);
        console.log("api details: ", data);
        })
    }

    const ingrediants = [];

    console.log(data.strIngredient1)
    console.log("affichage");

    return (
      <ScrollView>
        <ImageBackground source={require('../assets/background.jpg')} style={{ flex: 1, resizeMode: "cover", justifyContent: "center", width: '100%', }}>
          <View style={styles.containerDetail}>
            <Text style={styles.titreDetail}>{data.strDrink}</Text>
            <Image style={styles.img} source={{ uri : data.strDrinkThumb, }}/>
            <Text style={{color: '#45818e', marginBottom: 10,}}>Instructions :</Text>
            <Text>{data.strInstructions}</Text>
            <Text style={{color: '#45818e', marginTop: 10,}}>Ingredients :</Text>
            {data.strIngredient1 ? <View style={styles.ingredient}><Text>{data.strMeasure1} {data.strIngredient1}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient1}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient2 ? <View style={styles.ingredient}><Text>{data.strMeasure2} {data.strIngredient2}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient2}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient3 ? <View style={styles.ingredient}><Text>{data.strMeasure3} {data.strIngredient3}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient3}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient4 ? <View style={styles.ingredient}><Text>{data.strMeasure4} {data.strIngredient4}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient4}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient5 ? <View style={styles.ingredient}><Text>{data.strMeasure5} {data.strIngredient5}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient5}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient6 ? <View style={styles.ingredient}><Text>{data.strMeasure6} {data.strIngredient6}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient6}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient7 ? <View style={styles.ingredient}><Text>{data.strMeasure7} {data.strIngredient7}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient7}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient8 ? <View style={styles.ingredient}><Text>{data.strMeasure8} {data.strIngredient8}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient8}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient9 ? <View style={styles.ingredient}><Text>{data.strMeasure9} {data.strIngredient9}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient9}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient10 ? <View style={styles.ingredient}><Text>{data.strMeasure10} {data.strIngredient10}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient10}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient11 ? <View style={styles.ingredient}><Text>{data.strMeasure11} {data.strIngredient11}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient11}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient12 ? <View style={styles.ingredient}><Text>{data.strMeasure12} {data.strIngredient12}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient12}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient13 ? <View style={styles.ingredient}><Text>{data.strMeasure13} {data.strIngredient13}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient13}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient14 ? <View style={styles.ingredient}><Text>{data.strMeasure14} {data.strIngredient14}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient14}-Small.png`}}/></View> : <Text/>}
            {data.strIngredient15 ? <View style={styles.ingredient}><Text>{data.strMeasure15} {data.strIngredient15}</Text><Image style={styles.img_ingredient} source={{ uri : `https://www.thecocktaildb.com/images/ingredients/${data.strIngredient15}-Small.png`}}/></View> : <Text/>}
          </View>
        </ImageBackground>
      </ScrollView>
    );
  }

const styles = StyleSheet.create({

containerDetail: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    margin: 30,
    borderColor: 'black',
    borderWidth: 2,
    borderRadius: 10,
    backgroundColor:'white',
    color: '#45818e',

},
img: {
    width: 200,
    height: 200,
    marginTop: 5,
    marginBottom: 10,
},
titreDetail: {
    fontSize: 50,
    color: '#45818e',
},
img_ingredient: {
    width: 70,
    height: 70,
    justifyContent: 'center',
}, 
ingredient: {
  alignItems: 'center',
  marginTop: 10,
}
});

export default DetailsView