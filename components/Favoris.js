import { StatusBar } from 'expo-status-bar';
import { ImageBackground, FlatList, StyleSheet, Text, View, Button, Image, ScrollView, Pressable } from 'react-native';
import React, {useEffect, Component, useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const FavorisListView = (props) => {
    const FavorisItem = ({CocktailItem}) => {
        return(
            <View style={styles.favoris_list_container}>
                {
                    props.items && props.items.find(element => element == CocktailItem.idDrink) ?
                    <View style={styles.cocktail_list_item}>
                        <Text>{CocktailItem.strDrink}</Text>
                        <Image style={{width: 200, height: 200,}} source={{ uri : CocktailItem.strDrinkThumb }}/>
                        <Pressable onPress={ () => props.AddToFavorite(CocktailItem.idDrink)}>
                        <Image style={{ height: 25, width: 25, marginTop: 5,}} source={require('../assets/favorisselect.png')}/> 
                        </Pressable>
                    </View> : <View/>   
                                        
                }
            </View>
        )
    };
    var dataAPI = props.data;
    return(
        <ScrollView>
            <View style={styles.containerFavoris}>
                <ImageBackground source={require('../assets/background.jpg')} style={{ flex: 1, resizeMode: "cover", justifyContent: "center", width: '100%', }}>
                    <Text style={styles.titre}>Favoris</Text>
                    {
                        props.items && dataAPI ? 

                        dataAPI.map((data, idx) => {
                            return(
                            <FavorisItem key={idx} CocktailItem={data}/>
                            )
                        })
                        :
                        <View>
                            <Text>No favorite saved</Text>
                        </View>
                    }
                </ImageBackground>
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({

    titre: {
        alignSelf:'center',
        fontSize: 50,
        color: 'white',
    },
    containerFavoris: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },
    favoris_list_container: {
        paddingLeft:50,
        paddingRight:50,
    },
    img: {
        height: 50,
        weidth: 50,
    },
    cocktail_list_item: {
        fontSize: '20px',
        borderWidth: 2,
        borderColor: 'grey',
        borderRadius: 20,
        alignItems: 'center',
        paddingBottom: 5,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 5,
        marginTop: 20,
        backgroundColor: 'white',
    }
});

export default FavorisListView