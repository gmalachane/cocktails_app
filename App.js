import { StatusBar } from 'expo-status-bar';
import { ImageBackground, FlatList, StyleSheet, Text, View, Button, Image, Pressable } from 'react-native';
import React, {useEffect, useState} from 'react'; 
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import DetailsScreen from './components/detail';
import NavbarView from './components/Navbar';
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import FavorisScreen from './components/Favoris';
import FavorisListView from './components/Favoris';

export default function App() {
  const [data, setData] = useState();
  const [favoris, setFavoris] = useState([]);
  
  useEffect(() => {
    (async () => {
      fetchDataAPI();
    })();
  }, [])

  const fetchDataAPI = () => {
    fetch('https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a').then(res => res.json()).then(data => {
    setData(data.drinks);
    console.log("api2: ", data);
    })
  }
  

  function AddToFavorite(id) {
    if(favoris.indexOf(id) > -1 !=true) {
      setFavoris([...favoris, id]);
    } else {
      favoris.splice(favoris.indexOf(id), 1)
      setFavoris([...favoris]);
    }
  }
  
  function HomeScreen({ navigation }) {
    console.log("datacall :", data)
    const _renderItem = ({ item }) => <View style={styles.cocktail_list_item}><Pressable onPress={() => {navigation.navigate('Details', { id : item.idDrink })}}><Text style={{alignSelf: 'center', color: '#45818e',}}>{item.strDrink}</Text><Image style={styles.img} source={{ uri : item.strDrinkThumb, }}/></Pressable><Pressable onPress={ () =>AddToFavorite(item.idDrink)}>{favoris && favoris.find(element => element == item.idDrink) ?<Image style={{ height: 25, width: 25, marginTop: 5,}} source={require('./assets/favorisselect.png')}/> : <Image style={{ height: 25, width: 25, marginTop: 5,}} source={require('./assets/favorisvide.png')}/>}</Pressable></View>

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', margin: 0, }}>
        <ImageBackground source={require('./assets/background.jpg')} style={{ flex: 1, resizeMode: "cover", justifyContent: "center", width: '100%', }}>
        <Text style={styles.titre}>Liste de cocktails</Text>
        <FlatList data={data} renderItem={_renderItem} style={{paddingLeft: 50, paddingRight: 50,}}></FlatList>
        </ImageBackground>
      </View>
    );
  }

  function FavorisScreen({}) {
    return(
      <View>
        <FavorisListView data={data} items={favoris} AddToFavorite={AddToFavorite}></FavorisListView>
      </View>
    )
  }

  const Stack = createNativeStackNavigator();
  
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen}  initialParams={data}/>
        <Stack.Screen name="Details" component={DetailsScreen} />
        <Stack.Screen name="Favoris" component={FavorisScreen}/>
      </Stack.Navigator>
      <NavbarView></NavbarView>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titre: {
    fontSize: 40,
    alignSelf: 'center',
    color: 'white',
  },
  img: {
    width: 200,
    height: 200,
    marginTop: 5,
  },
  cocktail_list_item: {
    fontSize: '20px',
    borderWidth: 2,
    borderColor: 'grey',
    borderRadius: 20,
    alignItems: 'center',
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    marginTop: 20,
    backgroundColor: 'white',
  }
});
